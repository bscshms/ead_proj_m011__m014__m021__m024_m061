﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Data.Entity;
using Entities;
namespace DAL
{
    public class HospitalDBContext : DbContext
    {
        public DbSet<DailyReportDTO> DailyReport { get; set; }
        public DbSet<DonorDTO> Donor { get; set; }
        public DbSet<LoginDTO> Login { get; set; }
        public DbSet<StaffDTO> Staff { get; set; }
        public DbSet<StockDTO> Stock { get; set; }
        

    }
}