﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;
using System.Data;
using System.Data.Entity.Validation;
namespace DAL
{
   public class HosptialDBServices
    {
        public int SaveEmployee(LoginDTO dto)
        {
            using (var db = new HospitalDBContext())
            {
                db.Login.Add(dto);
                var result = db.SaveChanges();
                return result;
            }
        }
        public int Validate(LoginDTO dto)
        {
            using (var db = new HospitalDBContext())
            {
                var std = db.Login.Where(p => p.Username == dto.Username && p.Passwords == dto.Passwords).First();
                if (std != null)
                {
                    var result = db.SaveChanges();
                    return result;
                }
                else
                {
                    return -1;
                }
            }
        }
        public LoginDTO GetLogin(LoginDTO dto)
        {
            try
            {
                using (var dbCtx = new HospitalDBContext())
                {
                    return dbCtx.Login.Where(p => p.Username == dto.Username && p.Passwords == dto.Passwords).First(); 
                }
            }
            catch (InvalidOperationException e)
            {
               Console.WriteLine(e.Message);
               return null;
            }
        }
        public int AddStock(StockDTO dto)
        {
            using (var db = new HospitalDBContext())
            {
             
                int rec, iss, bal;
                Int32.TryParse(dto.Received, out rec);
                Int32.TryParse(dto.Issued, out iss);
                bal = rec - iss;

                dto.Balance = bal.ToString();
                db.Stock.Add(dto);
                var res = db.SaveChanges();
                return res;
            }
        }
        public List<StockDTO> GetStock()
        {
            using (var db = new HospitalDBContext())
            {
                var list = db.Stock.ToList();
                return list;
            }
        }
        public int dailyinsert(DailyReportDTO dto)
        {
            using (var db = new HospitalDBContext())
            {
                db.DailyReport.Add(dto);
                var result = db.SaveChanges();
                return result;
            }
        }
       public int delaccount(LoginDTO dto)
        {
            using (var db = new HospitalDBContext())
            {
                var std = db.Login.Where(p => p.Username == dto.Username).FirstOrDefault();
                if (std != null)
                {
                    db.Login.Remove(std);
                    var res = db.SaveChanges();
                    return res;
                }
                else
                {
                    return -1;
                }
            }
        }
       
        public List<DailyReportDTO> viewD()
        {
            using (var db = new HospitalDBContext())
            {
                var list = db.DailyReport.ToList();
                return list;
            }
        }
       public int updatepasswd(LoginDTO dto, String old, int id)
        {
            using (var db = new HospitalDBContext())
            {
                //var std = db.Login.Where(p => p.Username == dto.Username && p.Passwords == dto.Passwords).First();
               
                    var std = db.Login.Where(p => p.LoginID == id && p.Passwords == old ).First();
                    if (std != null)
                    {
                        std.Passwords = dto.Passwords;

                       var res = db.SaveChanges();
                       return res;
                    }
                
                else
                {
                    return -1;
                }
            }
        }
       public void DeleteStock(StockDTO dto)
       {
           using (var db = new HospitalDBContext())
           {
               var obj = db.Stock.Where(c => c.ItemID == dto.ItemID).FirstOrDefault();
               db.Stock.Remove(obj);
               db.SaveChanges();
           }
       }

       public void UpdateStock(StockDTO dto)
       {
           using (var db = new HospitalDBContext())
           {
               var obj = db.Stock.Where(c => c.ItemID == dto.ItemID).FirstOrDefault();
               obj.Name = dto.Name;
               obj.Received = dto.Received;
               obj.Issued = dto.Issued;
               int rec, iss, bal;
               Int32.TryParse(dto.Received, out rec);
               Int32.TryParse(dto.Issued, out iss);
               bal = rec - iss;
              
               dto.Balance = bal.ToString();
               obj.Balance = dto.Balance;
               db.SaveChanges();
           }
       }
       public int SaveDonor(DonorDTO dto)
       {
           using (var db = new HospitalDBContext())
           {
               db.Donor.Add(dto);
               var result = db.SaveChanges();
               return result;
           }
       }
       public List<LoginDTO> ViewAc()
       {
           using (var db = new HospitalDBContext())
           {
                var list = db.Login.ToList();
                return list;
           }
       }
       public void DeleteRep(DailyReportDTO dto)
       {
           int a = dto.ReportID;
           using (var db = new HospitalDBContext())
           {
               var obj = db.DailyReport.Where(c => c.ReportID == dto.ReportID).FirstOrDefault();
               db.DailyReport.Remove(obj);
               db.SaveChanges();
           }
       }
       public List<DailyReportDTO> GetDateData(DateTime dt, DateTime dt2)
       {
           try
           {
               using (var dbCtx = new HospitalDBContext())
               {
                   return dbCtx.DailyReport.Where(p => p.Date >= dt.Date && p.Date <= dt2.Date).ToList();
               }
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }
       public List<DonorDTO> BloodStock()
       {
           using (var db = new HospitalDBContext())
           {
               var result = db.Donor.ToList();
               return result;
           }
       }

       public void DeleteRecord(DonorDTO dto)
       {
           using (var db = new HospitalDBContext())
           {
               var obj = db.Donor.Where(c => c.DonorID == dto.DonorID).FirstOrDefault();
               db.Donor.Remove(obj);
               db.SaveChanges();
           }
       }

       public void UpdateRp(DailyReportDTO dto)
       {
           using (var db = new HospitalDBContext())
           {
               var obj = db.DailyReport.Where(p => p.ReportID == dto.ReportID).FirstOrDefault();
               obj.Grouping = dto.Grouping;
               obj.Screened = dto.Screened;
               obj.Bleed = dto.Bleed;
               obj.HBV = dto.HBV;
               obj.HCV = dto.HCV;
               obj.HIV = dto.HIV;
               obj.TP = dto.TP;
               obj.MP = dto.MP;
               obj.PCV = dto.PCV;
               obj.FFP = dto.FFP;
               obj.PLT = dto.PLT;
               obj.APHE = dto.APHE;
               db.SaveChanges();
           }
       }
       public void UpdateRecord(DonorDTO dto)
       {
           using (var db = new HospitalDBContext())
           {
               var obj = db.Donor.Where(c => c.DonorID == dto.DonorID).FirstOrDefault();
               obj.CNIC = dto.CNIC;
               obj.Name = dto.Name;
               obj.Age = dto.Age;
               obj.BloodGroup = dto.BloodGroup;
               obj.Phone = dto.Phone;
               db.SaveChanges();

           }
       }


    }
}
