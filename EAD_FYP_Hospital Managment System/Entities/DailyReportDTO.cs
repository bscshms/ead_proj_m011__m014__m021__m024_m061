﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Entities
{
    [Table("DailyReport")]
    public class DailyReportDTO
    {
        [Key]
        public int ReportID { get; set; }
        public DateTime? Date { get; set; }
        public int Grouping { get; set; }
        public int Screened { get; set; }
        public int Bleed { get; set; }
        public int HBV { get; set; }
        public int HCV { get; set; }
        public int HIV { get; set; }
        public int TP { get; set; }
        public int MP { get; set; }
        public int PCV { get; set; }
        public int FFP { get; set; }
        public int PLT { get; set; }
        public int APHE { get; set; }

    }
}