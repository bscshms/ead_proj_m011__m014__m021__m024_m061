﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities
{
 
    [Table("Donor")]
    public class DonorDTO
    {
        [Key]
        public int DonorID { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public string CNIC { get; set; }
        public string BloodGroup { get; set; }
        public string Phone {get;set;}
        public DateTime? Date {get;set;}

    }
}