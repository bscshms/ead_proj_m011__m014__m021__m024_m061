﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities
{
    [Table ("Login")]
    public class LoginDTO
    {
        [Key]
        public int LoginID { get; set; }
        public string Name { get; set; }
        public string Username { get; set; }
        public string Passwords { get; set; }
        public string JobTitle { get; set; }
        public DateTime? Date { get; set; }
    }
}