﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities
{
    [Table("Staff")]
    public class StaffDTO
    {
        [Key]
        public int StaffID { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public int CNIC { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Designation { get; set; }
        public DateTime Date { get; set; }
    }
}