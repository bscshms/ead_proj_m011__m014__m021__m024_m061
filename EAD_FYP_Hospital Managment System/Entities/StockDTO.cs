﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities
{
    [Table("Stock")]
    public class StockDTO
    {
        [Key]
        public int ItemID { get; set; }
        public string Name { get; set; }
        public string Received { get; set; }
        public string Issued { get; set; }
        public string Balance { get; set; }
        public DateTime? Date { get; set; }
    }
}