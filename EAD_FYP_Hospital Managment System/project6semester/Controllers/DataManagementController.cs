﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Entities;
using project6semester.Models;
namespace project6semester.Controllers
{
    public class DataManagementController : Controller
    {
        // GET: Data
        public ActionResult DonorData()
        {
            if (Session["UserName"] == null)
            {
                return Redirect("~/Home/SignIn");
            }
            return View();
        }
        public ActionResult StockData()
        {
            if (Session["UserName"] == null)
            {
                return Redirect("~/Home/SignIn");
            }
            return View();
        }
        /*public ActionResult AddStock(StockDTO dto)
        {
            if (Session["UserName"] == null)
            {
                return Redirect("~/Home/SignIn");
            }
            BAL obj = new BAL();
            int rec, iss, bal;
            Int32.TryParse(dto.Received, out rec);
            Int32.TryParse(dto.Issued, out iss);
            bal = rec - iss;
            dto.Balance = bal.ToString();
            obj.SaveStock(dto);
            return View("StockData");
        }*/
        [HttpPost]
        public JsonResult AddStock(StockDTO dto)
        {
           
            BAL obj = new BAL();
            int cid = obj.SaveStock(dto);

            Object obj1 = new
            {
                CustomerID = cid,
                success = true,
                message = "Successfully Saved"
            };

            return Json(obj1);
        }
        public ActionResult GetStock()
        {
            if (Session["UserName"] == null)
            {
                return Redirect("~/Home/SignIn");
            }
            BAL obj = new BAL();
            var list = obj.GetAllStock();
           // var a = list[1].Name;
            return View("AddStock", list);
        }
        public ActionResult Update(StockDTO dto)
        {
            if (Session["UserName"] == null)
            {
                return Redirect("~/Home/SignIn");
            }
            int id = dto.ItemID;
            return View("UpdateStock", dto);

        }

        public ActionResult UpdateStock(StockDTO dto)
        {
            if (Session["UserName"] == null)
            {
                return Redirect("~/Home/SignIn");
            }
            string name = Request["Name"];
            dto.Name = name;
            int id = dto.ItemID;
            BAL obj = new BAL();
            obj.UpdateStock(dto);
            var list = obj.GetAllStock();
            return View("AddStock", list);
        }
        public ActionResult Delete()
        {
            if (Session["UserName"] == null)
            {
                return Redirect("~/Home/SignIn");
            }
            BAL obj = new BAL();
            var dto = obj.GetAllStock();
            return View("DeleteStock", dto);
        }
        public ActionResult DeleteStock(StockDTO dto)
        {
            if (Session["UserName"] == null)
            {
                return Redirect("~/Home/SignIn");
            }
            BAL obj = new BAL();
            obj.Delete(dto);
            var list = obj.GetAllStock();
            return View("DeleteStock", list);
        }

       /* public ActionResult AddDonor(DonorDTO dto2)
        {
            if (Session["UserName"] == null)
            {
                return Redirect("~/Home/SignIn");
            }
            BAL obj = new BAL();
            int res = obj.SaveDonor(dto2);
            if (res != 0)
                return View("DonorData");
            else
                return View("StockData");
        }*/
        [HttpPost]
        public JsonResult AddDonor(DonorDTO dto)
        {
          
            BAL obj = new BAL();
            int cid = obj.SaveDonor(dto);

            Object obj1 = new
            {
                CustomerID = cid,
                success = true,
                message = "Successfully Saved"
            };

            return Json(obj1);
        }
        public ActionResult BloodStock()
        {
            if (Session["UserName"] == null)
            {
                return Redirect("~/Home/SignIn");
            }
            BAL obj = new BAL();
            var list = obj.BloodStock();
            //var ss = list[1].Name;
            return View("BloodStock", list);
        }

        public ActionResult UpdateBlood(DonorDTO dto)
        {
            if (Session["UserName"] == null)
            {
                return Redirect("~/Home/SignIn");
            }
            return View("UpdatePage", dto);
        }


        public ActionResult Updates(DonorDTO dto)
        {
            if (Session["UserName"] == null)
            {
                return Redirect("~/Home/SignIn");
            }
            BAL obj = new BAL();
            obj.UpdateBlood(dto);
            var list = obj.BloodStock();
            return View("BloodStock", list);
        }

        public ActionResult DeleteBloodStock()
        {
            if (Session["UserName"] == null)
            {
                return Redirect("~/Home/SignIn");
            }
            BAL obj = new BAL();
            var list = obj.BloodStock();
            return View("DeleteBloodView", list);
        }

        public ActionResult DeleteBloodEntry(DonorDTO dto)
        {
            if (Session["UserName"] == null)
            {
                return Redirect("~/Home/SignIn");
            }
            BAL obj = new BAL();
            obj.DeleteBlood(dto);
            var list = obj.BloodStock();
            return View("DeleteBloodView", list);
        }
        public ActionResult ViewReport()
        {
            if (Session["UserName"] == null)
            {
                return Redirect("~/Home/SignIn");
            }
            BAL obj = new BAL();
            var list = obj.getAllData();
            return View("ViewReport", list);
        }

        public ActionResult DeleteReport()
        {
            if (Session["UserName"] == null)
            {
                return Redirect("~/Home/SignIn");
            }
            BAL obj = new BAL();
            var list = obj.getAllData();
            return View("DeleteReport", list);
        }


        public ActionResult DeleteRepEntry(DailyReportDTO dto)
        {
            if (Session["UserName"] == null)
            {
                return Redirect("~/Home/SignIn");
            }
            BAL obj = new BAL();
            obj.DeleteRep(dto);
            var list = obj.getAllData();
            return View("DeleteReport", list);
        }

        public ActionResult wrtDate()
        {
            if (Session["UserName"] == null)
            {
                return Redirect("~/Home/SignIn");
            }
            BAL obj = new BAL();
            string dateTime = Request["DateFrom"];
            string dateTo = Request["DateTo"];
            DateTime dt;
            DateTime dt2;

            if (dateTo == "" && dateTime == "")
            {
                var list = obj.getAllData();
                return View("ViewReport", list);
            }
            else if (dateTo == "")
            {
                dt = Convert.ToDateTime(dateTime);
                dt2 = DateTime.Now;
            }

            else if (dateTime == "")
            {
                string d = "1900-05-11";
                dt = Convert.ToDateTime(d);
                dt2 = Convert.ToDateTime(dateTo);
            }

            else
            {
                dt = Convert.ToDateTime(dateTime);
                dt2 = Convert.ToDateTime(dateTo);
            }
            var list2 = obj.getAllDataDate(dt, dt2);
            return View("ViewReport", list2);
        }



        public ActionResult UpdateRep(DailyReportDTO dto)
        {
            if (Session["UserName"] == null)
            {
                return Redirect("~/Home/SignIn");
            }
            return View("UpdateReport", dto);
        }
        public ActionResult UpdateReport(DailyReportDTO dto)
        {
            if (Session["UserName"] == null)
            {
                return Redirect("~/Home/SignIn");
            }
            BAL ob = new BAL();
            ob.updaterep(dto);
            var list = ob.getAllData();
            return View("ViewReport",list);
        }
    }
}