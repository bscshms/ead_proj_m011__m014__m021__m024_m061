﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Entities;
using project6semester.Models;

namespace project6semester.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult ViewAccount()
        {
           BAL rp = new BAL();
            string title = Session["job"].ToString();
           
            if (title != "admin")
            {
                return Redirect("~/Home/SignIn");
            }
            var list = rp.ViewAc();
            return View("ViewAccount",list);

        }
        public ActionResult DailyReport ()
        {
            if (Session["UserName"] == null)
            {
                return Redirect("~/Home/SignIn");
            }
            return View();
        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SignIn()
        {
            LoginDTO dto = new LoginDTO();

            if (Request.Cookies["username"] != null)
            {
                dto.Username= Request.Cookies["username"].Value;
            }
            if (Request.Cookies["password"] != null)
            {
                dto.Passwords = Request.Cookies["password"].Value;
            }
            

            return View(dto);
        }
        [HttpPost]
        [ActionName("SignIn")]
        public ActionResult Validate(LoginDTO dto)
        {
            
            var p = dto.Username;
            dto.Passwords = Request["password"];
            var t = dto.Passwords;
            BAL re = new BAL();
            if (dto.Username == "" || dto.Passwords == "")
            {
                ViewBag.Error = "User/Password is Invalid";
                return Redirect("~/Home/SignIn");
            }
            var dt = re.GetLogin(dto);
            if (dt==null)
            {
                LoginDTO dtoo = new LoginDTO();
                dtoo.Username = "Invalid username and Password ";

                return RedirectToAction("SignIn",dtoo);

            }
            if (re.Validate(dto)!=-1)
            {



                Session["UserName"] = dto.Username;
                Session["job"] = dt.JobTitle;
                Session["id"] = dt.LoginID;
                var route = new
                {
                    controller = "Home",
                    action = "Insert"
                };
               
                
                return RedirectToRoute(route);

                //return RedirectToAction("Dashboard", "Home");

                //return Redirect("~/Home/Dasbhoard")
            }
            else
            {
                dto.Passwords = "";
                ViewBag.Error = "User/Password is Invalid";
            }

            return View("SignIn", dto);
        }
        public ActionResult Insert()
        {
            if (Session["UserName"]==null)
            {
                return Redirect("~/Home/SignIn");
            }
            return View();
        }
        public ActionResult SignUp()
        {
            if (Session["job"] == null)
            {
                return Redirect("~/Home/SignIn");
            }
            else{
            string title = Session["job"].ToString();
            if (title != "admin")
            {
                return Redirect("~/Home/SignIn");
            }
            }
            return View();
        }
        [HttpGet]
        public ActionResult Change()
        {
            return View();
        }
        [HttpPost]
        [ActionName("Change")]
        public ActionResult chan(LoginDTO dto)
        {
            dto.Passwords = Request["passwordn"];
            dto.Username = Request["username"];
            var old = Request["password"];
            string idd = Session["id"].ToString();
            int id ;
             Int32.TryParse(idd,out id);
             BAL rp = new BAL();
             if (rp.changepswd(dto, old, id) != -1)
             {
                return Redirect("~/Home/Insert");
             }
             else
             {
                 return View();
             }
        }
        public ActionResult Delete()
        {

            string title = Session["job"].ToString();
            if (title != "admin")
            {
                return Redirect("~/Home/SignIn");
            }
            return View();
        }
        [HttpPost]
        [ActionName("Delete")]
        public ActionResult del(LoginDTO dto)
        {
            
            dto.Username = Request["username"];
           
            BAL rp = new BAL();
            if (rp.del(dto)!=-1)
            {
                return Redirect("~/Home/Insert");
            }
            else{
                return View();
            }
        }
        [HttpGet]
        public ActionResult Logout()
        {
            Session["job"]=null;
            Session["UserName"] = null;
            Session["id"] = null;
            Session.Abandon();

            return Redirect("~/Home/SignIn");
        }
        [HttpPost]
        public JsonResult SaveEmployee(LoginDTO dto)
        {
            string p = dto.Passwords;
            BAL repo = new BAL();
            int cid = repo.SaveEmployee(dto);

            Object obj = new
            {
                CustomerID = cid,
                success = true,
                message = "Successfully Saved"
            };
          
            return Json(obj);
        }
        [HttpPost]
        public JsonResult dailyinsert(DailyReportDTO dto)
        {
            BAL repo = new BAL();

            int cid = repo.dailyinsert(dto);

            Object obj = new
            {
                CustomerID = cid,
                success = true,
                message = "Successfully Saved"
            };

            return Json(obj);
        }


    }
}