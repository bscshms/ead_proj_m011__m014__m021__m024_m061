﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Entities;
using DAL;
namespace project6semester.Models
{
    public class BAL
    {
        public int SaveEmployee(LoginDTO dto)
        {
            HosptialDBServices dataService = new HosptialDBServices();
            int cid = dataService.SaveEmployee(dto);
            return cid;
        }
        public int Validate (LoginDTO dto)
        {
            HosptialDBServices dataService = new HosptialDBServices();
            int cid = dataService.Validate(dto);
            return cid;
        }
        public LoginDTO GetLogin (LoginDTO dto)
        {
            HosptialDBServices dataService = new HosptialDBServices();
            dto = dataService.GetLogin(dto);
            return dto;
        }
        public int SaveStock(StockDTO dto)
        {
            HosptialDBServices dataService = new HosptialDBServices();
            int cid = dataService.AddStock(dto);
            return cid;
        }
        public List<StockDTO> GetAllStock()
        {
            HosptialDBServices ds = new HosptialDBServices();
            return ds.GetStock();
        }
        public int dailyinsert(DailyReportDTO dto)
        {
            HosptialDBServices dataService = new HosptialDBServices();
            int cid = dataService.dailyinsert(dto);
            return cid;
        }
        public List<DailyReportDTO> getAllData()
        {
            HosptialDBServices dataService = new HosptialDBServices();
            return dataService.viewD();


        }
        public int changepswd(LoginDTO dto, string old, int id)
        {
            HosptialDBServices dataService = new HosptialDBServices();
            int cid = dataService.updatepasswd(dto,old,id);
            return cid;
        }
        public int del(LoginDTO dto)
        {
            HosptialDBServices dataService = new HosptialDBServices();
            int cid = dataService.delaccount(dto);
            return cid;
        }
        public void UpdateStock(StockDTO dto)
        {
            HosptialDBServices ds = new HosptialDBServices();
            ds.UpdateStock(dto);
        }
        public void DeleteRep(DailyReportDTO dto)
        {
            HosptialDBServices dataservice = new HosptialDBServices();
            dataservice.DeleteRep(dto);
        }
        public List<LoginDTO> ViewAc()
        {
            HosptialDBServices dataservice = new HosptialDBServices();
            return dataservice.ViewAc();
        }
        public void Delete(StockDTO dto)
        {
            HosptialDBServices ds = new HosptialDBServices();
            ds.DeleteStock(dto);
        }
        public int SaveDonor(DonorDTO dto)
        {
            HosptialDBServices dataService = new HosptialDBServices();
            int cid = dataService.SaveDonor(dto);
            return cid;
        }
        public List<DailyReportDTO> getAllDataDate(DateTime dt, DateTime dt2)
        {
            HosptialDBServices dataService = new HosptialDBServices();
            return dataService.GetDateData(dt, dt2);


        }
        public List<DonorDTO> BloodStock()
        {
            HosptialDBServices dataService = new HosptialDBServices();
            return dataService.BloodStock();

        }

        public void UpdateBlood(DonorDTO dto)
        {
            HosptialDBServices dataService = new HosptialDBServices();
            dataService.UpdateRecord(dto);

        }

        public void DeleteBlood(DonorDTO dto)
        {
            HosptialDBServices dataService = new HosptialDBServices();
            dataService.DeleteRecord(dto);

        }
        public void updaterep(DailyReportDTO dto)
        {
            HosptialDBServices dataservice = new HosptialDBServices();
            dataservice.UpdateRp(dto);
        }
    }
}