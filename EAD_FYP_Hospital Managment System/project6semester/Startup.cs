﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(project6semester.Startup))]
namespace project6semester
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
